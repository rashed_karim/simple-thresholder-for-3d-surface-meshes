#define HAS_VTK 1
#define _IS_DEBUG 1

#include "vtkPointData.h"
#include <vtkPointPicker.h>
#include <vtkCommand.h>
#include <vtkMarchingCubes.h>
#include <vtkContourFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkPolyDataMapper.h>
#include <vtkCamera.h>
#include <vtkMarchingCubes.h>
#include <vtkVectorNorm.h>
#include <vtkDataSetMapper.h>
#include <vtkImageToPolyDataFilter.h>
#include <vtkPolyDataReader.h>
#include <vtkLookupTable.h>
#include <vtkSphereSource.h>
#include <vtkCallbackCommand.h>
#include <vtkProperty.h>
#include <vtkImagePlaneWidget.h>
#include <vtkImageActor.h>
#include <vtkSmartPointer.h>
#include <vtkCellArray.h>
#include <vtkPolyDataWriter.h>
#include <vtkCellData.h>
#include <vtkPolyDataReader.h>
#include <vtkIterativeClosestPointTransform.h>
#include <vtkLandmarkTransform.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkMaskPoints.h>
#include <vtkFloatArray.h>


#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;


  
/*
*   Reads scalars from a polydata shell  
*/
void SuppressScalars(char* poly_fn, char* poly_out_fn, double t1)
{
  double min=1e9, max=-1;
  vtkSmartPointer<vtkPolyData> surface =vtkSmartPointer<vtkPolyData>::New();  
  vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New(); 
  reader->SetFileName(poly_fn); 
  reader->Update();
  
  surface = reader->GetOutput();
  
  vtkSmartPointer<vtkFloatArray> scalars = vtkSmartPointer<vtkFloatArray>::New();
  scalars = vtkFloatArray::SafeDownCast(surface->GetPointData()->GetScalars());  
  double count = 0;

   vtkSmartPointer<vtkFloatArray> scalars_out = vtkSmartPointer<vtkFloatArray>::New();

   

  
  for (int k=0;k<surface->GetNumberOfPoints();k++)
  {
    double s = scalars->GetTuple1(k); 
    
    if (s > t1)
    {
		 scalars_out->InsertNextTuple1(k);
    }
	else {
		scalars_out->InsertNextTuple1(s);
	}
    
  }
  
   surface->GetPointData()->SetScalars(scalars_out); 
  
  vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New(); 
  writer->SetInputData(surface);
  writer->SetFileName(poly_out_fn); 
  writer->Update();
  
  
  // Some debug to output the range of scalars
  //cout << "Min scalar = " << min << "\nMax scalar = " << max << endl;
}

int main(int argc, char **argv)
{
  
  bool write_to_file = false; 
  vector<double> scalar;
  char* poly_fn, *poly_out_fn;
  char* out_fn;  
  
  double t1;
  
  if (argc < 4)
  {
     cerr << "Not enough parameters\nUsage: <file_in.vtk> <file_out.vtk> <t1>"<< endl; 
     exit(1);
  }
  else if (argc == 5)
  {
    write_to_file = true;
    out_fn = argv[4];  
  }
  
  poly_fn = argv[1];
  poly_out_fn = argv[2];
  t1 = atof(argv[3]);
  //t2 = atof(argv[3]);
   
  
	SuppressScalars( poly_fn,poly_out_fn,t1);
  
  
}