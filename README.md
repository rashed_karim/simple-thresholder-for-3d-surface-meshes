# Introduction 
T_scalar simply thresholds the scalar of a VTK polydata containing Point scalars 

### What is this repository for? ###

* Threshold Point Data scalars to 0 that are above a certain value 
* Version 1.0 

### Usage ###

The program expects an input file containing **Point Data** scalars. The threshold value is specified in **t1**

```
#!c++

t_scalar <file_input_vtk> <file_output_vtk> <threshold t1> 
```



### Who do I talk to? ###

* Rashed Karim, rashed.karim@kcl.ac.uk